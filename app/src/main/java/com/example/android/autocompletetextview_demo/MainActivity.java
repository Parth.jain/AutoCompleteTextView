package com.example.android.autocompletetextview_demo;

import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    String[] fruits = {"Apple","Appricodes","Avocrados", "Banana","Bluberry", "Cherry","Cucumbers", "Date", "Grape", "Kiwi", "Mango", "Pear","Pineapple"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ArrayAdapter<String> adapter=new ArrayAdapter<String>(this,android.R.layout.select_dialog_item,fruits);

        AutoCompleteTextView mAutoCompleteFruitText=findViewById(R.id.autoTextView);

        mAutoCompleteFruitText.setThreshold(1);

        mAutoCompleteFruitText.setAdapter(adapter);

        mAutoCompleteFruitText.setTextColor(Color.RED);

        TextView mFruits=findViewById(R.id.FruitText);

        mFruits.setText("Apple, Appricodes, Avocrados, Bannas, Bluberry, Cherry,Cucumbers, Date, Grape, Kiwi, Pineapple, Pear");
        mFruits.setTextSize(20);
    }
}
